
#pragma once

#include <windows.h>
#include <stdio.h>

namespace fs
{
    //
    // 简单实现的仿[string]类
    //
    // 没有支持多线程

    class String
    {
    public:
        // 初始化属性
        void init()
        {
            pString = new char[1];
            pString[0] = 0;
            pTmpString = NULL;
            pRetString = NULL;
        }

        // 构造
        String() {
            //printf("String()\n");
            init();
        }

        String(const char* str) {
            //printf("String(char*='%s')\n",str);
            if (str) {
                int len = strlen(str);
                pString = new char[len + 1];
                pString[len] = 0;
                strcpy(pString, str);
                pTmpString = NULL;
                pRetString = NULL;
            }
            else {
                init();
            }
        }

        //如果直接使用这个，类似这样： String( tmp=new String ) 就内存泄露了
        //所以重载什么的全部使用了 String(const char* str);  除了 String = String2 这样的
        String(String &str) {
            //printf("String(String='%s')\n",str.c_str());
            init();
            *this += str;
        }

        ~String() {
            //printf("~String [%s]\n",pString);
            if (pTmpString)
                delete pTmpString;
            if (pRetString)
                delete pRetString;
            if (pString)
                delete pString;
        }


        // 方法
        const char* c_str() {
            return pString;
        }

        const int length() {
            return strlen(pString);
        }

        void clear() {
            memset(pString, 0, strlen(pString));
        }

        bool empty() {
            return (pString[0] == 0);
        }

        const char* find(const char* substr) {
            return strstr(pString, substr);
        }

        const char* rfind(const char* substr) {
            if (substr) {
                int sublen = strlen(substr);
                int len = strlen(pString);
                for (int i = len; i > 0; i--) {
                    if (pString[i] == *substr && strncmp(&pString[i], substr, sublen) == 0)
                        return &pString[i];
                }
            }
            return NULL;
        }

        // 使用vsprintf实现的format 不支持类 只能传入基本类型
        String &format(const char* ft, ...) {
            char szBuf[1024] = { 0 };
            va_list arg = NULL;
            va_start(arg, ft);
            vsprintf_s(szBuf, sizeof(szBuf), ft, arg);
            va_end(arg);
            return (*this = szBuf);
        }

        // 专用方法
        //
        // getdir 返回当前路径字符串的目录 最后带有'\\'
        const char* getdir() {
            String str = pString;
            char* tmp = (char*)str.rfind("\\");
            if (tmp) {
                tmp[1] = 0;

                if (pRetString)
                    delete pRetString;
                pRetString = new char[str.length() + 1];
                strcpy(pRetString, str.c_str());
                return pRetString;
            }
            return NULL;
        }
        //
        // getfilename 返回当前路径字符串的文件名
        const char* getfilename() {
            const char* tmp = this->rfind("\\");
            if (tmp) {
                tmp++;

                if (pRetString)
                    delete pRetString;
                pRetString = new char[strlen(tmp) + 1];
                strcpy(pRetString, tmp);
                return pRetString;
            }
            return NULL;
        }

        // 重载运算符
        String &operator=(const char* str) {
            if (str) {
                if (pString)
                    delete pString;

                int len = strlen(str);
                pString = new char[len + 1];
                pString[len] = 0;
                strcpy(pString, str);
            }
            return *this;
        }

        String &operator=(String &str) {
            *this = str.c_str();
            return *this;
        }

        String &operator+=(const char* str) {
            if (str) {
                int len = strlen(str);
                if (pString&&len) {
                    int orilen = strlen(pString);
                    char* newString = new char[len + orilen + 1];
                    newString[len + orilen] = 0;

                    strcpy(newString, pString);
                    strcat(newString, str);
                    delete pString;

                    pString = newString;
                }
            }
            return *this;
        }

        String &operator+=(String &str) {
            return *this += str.c_str();
        }

        const char* operator+(const char* str) {
            int len = 0;
            if (str)
                len = strlen(str);
            if (pTmpString)
                delete pTmpString;

            int oldlen = strlen(pString);
            pTmpString = new char[oldlen + len + 1];
            pTmpString[oldlen + len] = 0;
            wsprintfA(pTmpString, "%s%s", pString, str);
            return pTmpString;
        }

        const char* operator+(String &str) {
            return (*this + str.c_str());
        }

        char &operator[](int n) {
            return (pString[n]);
        }

        // _strcmpi实现 无视大小写
        bool operator==(const char* str) {
            return (_strcmpi(pString, str) == 0);
        }
        bool operator==(String &str) {
            return (_strcmpi(pString, str.c_str()) == 0);
        }
        bool operator!=(const char* str) {
            return (_strcmpi(pString, str) != 0);
        }
        bool operator!=(String &str) {
            return (_strcmpi(pString, str.c_str()) != 0);
        }


        // 友元  
        // char* + String
        friend const char* operator+(const char* str, String &strclass) {
            int len = 0;
            if (str)
                len = strlen(str);
            if (strclass.pTmpString)
                delete strclass.pTmpString;

            int oldlen = strlen(strclass.pString);
            strclass.pTmpString = new char[oldlen + len + 1];
            strclass.pTmpString[oldlen + len] = 0;
            wsprintfA(strclass.pTmpString, "%s%s", str, strclass.pString);
            return strclass.pTmpString;
        }

        //  char* == String
        friend bool operator==(const char* str, String &strclass) {
            return (strclass == str);
        }
        friend bool operator!=(const char* str, String &strclass) {
            return (strclass != str);
        }

    private:
        // 字符串地址
        char* pString;
        // 为了防止内存泄露而设立的固定的地址
        char* pTmpString;
        // 专用方法返回字符串时 例如ret pRetString<- getdir()
        char* pRetString;
    };
}

