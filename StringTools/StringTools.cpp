// StringTools.cpp : 定义控制台应用程序的入口点。
#include "stdafx.h"
#include <stdlib.h>
#include <windows.h>
#include "String.h"
#include "resource.h"


/************************************************************************/
/* 拆分字符串成为字节序列
/************************************************************************/
VOID SetStrToBytes(HWND hDlg)
{
    char str[200];
    GetDlgItemTextA(hDlg, IDC_EDIT1, str, 200);

    if (str[0]) {
        fs::String buf;
        buf += "{";

        PBYTE pbtr = (PBYTE)str;
        int i = 0;
        while (pbtr[i] != 0) {
            char tmp[10];
            wsprintfA(tmp, "0x%02X", pbtr[i]);

            buf += tmp;
            buf += ",";

            i++;
        }

        buf += "0};";
        SetDlgItemTextA(hDlg, IDC_EDIT2, buf.c_str());
    }
}

VOID SetWStrToBytes(HWND hDlg)
{
    WCHAR str[200];
    GetDlgItemTextW(hDlg, IDC_EDIT1, str, 200);

    if (str[0]) {
        fs::String buf;
        buf += "{";

        PBYTE pbtr = (PBYTE)str;
        int i = 0;
        while (1) {
            if (pbtr[i] == 0 && pbtr[i + 1] == 0)
                break;

            char tmp[10];
            wsprintfA(tmp, "0x%02X", pbtr[i]);
            buf += tmp;
            buf += ",";

            i++;
        }

        buf += "0,0};";
        SetDlgItemTextA(hDlg, IDC_EDIT3, buf.c_str());
    }
}

VOID SetStrToEMIT(HWND hDlg)
{
    char str[200];
    GetDlgItemTextA(hDlg, IDC_EDIT1, str, 200);

    if (str[0]) {
        fs::String buf;

        PBYTE pbtr = (PBYTE)str;
        int i = 0;
        while (pbtr[i] != 0) {

            char tmp[20];
            wsprintfA(tmp, "_EMIT 0x%02X \r\n", pbtr[i]);
            buf += tmp;

            i++;
        }

        buf += "_EMIT 0x00";
        SetDlgItemTextA(hDlg, IDC_EDIT4, buf.c_str());
    }
}



/************************************************************************/
/* 运行框
/************************************************************************/
BOOL CALLBACK RunWindowProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_COMMAND:
        if (HIWORD(wParam) == EN_CHANGE) {
            if (LOWORD(wParam) == IDC_EDIT1) {
                SetStrToBytes(hDlg);
                SetWStrToBytes(hDlg);
                SetStrToEMIT(hDlg);
            }
        }
        return 1;

    case WM_CLOSE:
        EndDialog(hDlg, 0);
        return 1;
    }

    return 0;
}

int WINAPI WinMain(__in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd)
{
    DialogBoxA(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), (HWND)0, RunWindowProc);
    return 0;
}
